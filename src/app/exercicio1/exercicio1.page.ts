import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exercicio1',
  templateUrl: './exercicio1.page.html',
  styleUrls: ['./exercicio1.page.scss'],
})
export class Exercicio1Page implements OnInit {

  public numeroPassos: number;
  public passos: string;

  constructor() {
    this.numeroPassos = 0;
    this.passos = '';
   }

  ngOnInit() {
  }

  public contarVales(): number {
    const saida = 0;
    const validacoes = {
      numeroPassos: this.validarNumeroPassos(this.numeroPassos),
      qtdPassos: this.validarQtdPassos(this.numeroPassos, this.passos),
      direcoesPassos: this.validarDirecoesPassos(this.passos)
    };
    console.log('Validações: ', validacoes);
    return saida;
  }

  /**
   * Valida se numeroPassos está entre 2 e 1000000.
   * @param {number} numeroPassos numero de passos dados
   */
  public validarNumeroPassos(numeroPassos: number): boolean {
    console.log('Validar número passos: ');
    console.log('Numero passos: ', numeroPassos);
    console.log('Saída', numeroPassos >= 2 && numeroPassos <= 1000000 );
      return numeroPassos >= 2 && numeroPassos <= 1000000 ;
  }

  /**
   * Valida se o numeroPassos é o mesmo valor da quantidade de letras em passos.
   * @param {number} numeroPassos numero de passos dados 
   * @param {passos} passos comando dos passos
   */
  public validarQtdPassos(numeroPassos: number, passos: string): boolean {
    console.log('Validar quantidade de passos: ');
    console.log('Número passos: ', numeroPassos);
    console.log('Passos: ', passos, 'Tamanho: ', passos.length);
    console.log('Saída: ', numeroPassos === passos.length);

    return numeroPassos === passos.length;
  }

  /**
   * Valida as direções dos passos Up e Down
   * @param {string} passos comando dos passos
   */
  public validarDirecoesPassos(passos: string): boolean {
    passos = passos.toLowerCase();
    console.log('Validar Direções dos Passos ');
    console.log('Passos: ', passos);
    const listaPassos = passos.split('');
    let saida = true;

    for (let indice = 0; indice < listaPassos.length; indice ++) {
      if ( listaPassos[indice] !== 'u' && listaPassos[indice] !== 'd') {
        saida = false;
      }
    }
    console.log('Saída: ', saida);
    return saida;
  }

}
