import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Exercicio1Page } from './exercicio1.page';

const routes: Routes = [
  {
    path: '',
    component: Exercicio1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Exercicio1Page]
})
export class Exercicio1PageModule {}
